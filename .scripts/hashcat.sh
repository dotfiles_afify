#!/usr/bin/env bash

# hashcat -m 2500 -a 3 $1 ?d?d?d?d?d?d?d?d -o /tmp/result.txt
# hashcat -m 2500 -a 3 $1 ?a?a?a?a?a?a?a?a -o /tmp/result.txt
hashcat -m 2500 -a 3 $1 ?l?l?l?l?l?l?l?l -o /tmp/result.txt

hashcat -m 0 -a 3 --session session_name example0.hash masks/rockyou-7-2592000.hcmask
hashcat --session session_name --restore

