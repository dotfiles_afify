#!/usr/bin/env bash

filename=$1
scale=720
# scale=480
# skip="-ss 30"
# output_total_sec="-t 3" 

# ffmpeg $skip $output_total_sec-i $filename -vf "fps=10,scale=$scale:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 $filename.gif

ffmpeg -i "$filename" -vf scale=$scale:-1 "$filename.gif"
