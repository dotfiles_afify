#!/usr/bin/env bash

phrase=`cat /etc/services |  \
  grep -v ^\s*# |  \
  grep -v ^\s*$ | \
  sed -e's/\s.*//' | \
  sort -ui | \
  dmenu -i -l 20`

rtrn=$?

if test "$rtrn" = "0"; then
  echo "You chose servicename $phrase"
else
  echo "You aborted."
fi
