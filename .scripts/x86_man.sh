#!/bin/sh

git clone https://github.com/ttmo-O/x86-manpages && cd x86-manpages
doas mkdir -p /usr/local/man/man7
doas cp man7/* /usr/local/man/man7/
cd .. && rm -rf x86-manpages
