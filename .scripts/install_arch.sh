#!/usr/bin/env sh

disk partition

mount root to /mnt
mount data to /mnt/data

pacstrap /mnt base base-devel vim

genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt
edit fstab data mount point and discard in root

time zone

install essential packages

config grub

root password
add new user
visudo


systemd services
NetworkManager
bluetooth

set hostname



create_mnt_dirs
add_data_to_fstab

copy security ssh gpg pass
copy dotfiles .scripts

gkt config
- copy themes files
- create config files to home

disable_intel from loading and use nvidia
disable systemd default to mulit-user.target

each program configuration
- dwm
install_suckless
	add dwm config
- st
- surf
- dmenu
- vim
	install vim plug
	PlugInstall
	patch gruvbox
- vifm
	install yaourt
	install nerd-fonts-complete
	sudo pip install ueberzug
- slock
- mutt
	add mutt config
	sudo pacman 
- ...

sudo pacman -S wireshark-qt wireshark-cli
sudo usermod -a -G wireshark hassan
