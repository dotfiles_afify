#!/bin/sh

html_file=$1
output_pdf=$html_file.pdf
page_size='A4'

wkhtmltopdf -s "$page_size" "$html_file" "$output_pdf"
