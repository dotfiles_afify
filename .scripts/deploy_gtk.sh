#!/bin/sh

confdir='/mnt/data/resources'
dist="$HOME/.local/share/themes/Mojave-dark/"

[ ! -d "$dist" ] && mkdir -p "$dist" && cp -R $confdir/Mojave-dark/* "$dist"

[ ! -f "$HOME/.config/gtk-2.0/.gtkrc-2.0" ] && mkdir -p "$HOME/.config/gtk-2.0" \
	&& echo "gtk-theme-name = Mojave-dark
gtk-font-name = Terminus 12" > "$HOME/.config/gtk-2.0/.gtkrc-2.0"

[ ! -f "$HOME/.config/gtk-3.0/settings.ini" ] && mkdir -p "$HOME/.config/gtk-3.0" \
	&& echo "[Settings]
	gtk-application-prefer-dark-theme = true
	gtk-theme-name = Mojave-dark
	#gtk-font-name = Terminus 12" > "$HOME/.config/gtk-3.0/settings.ini"
