#!/bin/sh

case $(uname) in
	Linux) doas xbps-install -Syu && doas xbps-remove -Ooy;;
	OpenBSD) doas fw_update && doas sysupgrade -s && doas pkg_add -Uu && doas pkg_delete -a && pkg_check;;
esac
