#!/bin/sh

echo "hw.allowpowerdown=0" > /etc/sysctl.conf
rcctl enable apmd
rcctl set apmd flags -A
rcctl start apmd
rcctl disable smtpd
rcctl disable sshd
sysupgrade -s
