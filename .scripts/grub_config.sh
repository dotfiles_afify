#!/bin/sh

echo "GRUB_DEFAULT=0
GRUB_TIMEOUT=0
GRUB_RECORDFAIL_TIMEOUT=$GRUB_TIMEOUT
GRUB_DISTRIBUTOR=\"Void\"
GRUB_CMDLINE_LINUX_DEFAULT=\"loglevel=4 slub_debug=P page_poison=1\"
GRUB_TERMINAL_INPUT=console
GRUB_TERMINAL_OUTPUT=console"> /etc/default/grub

grub-mkconfig -o /boot/grub/grub.cfg
