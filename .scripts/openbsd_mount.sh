#!/usr/bin/env bash

dmesg | egrep '^(cd|wd|sd|fd). at '
sysctl hw.disknames
sysctl hw.diskcount
sysctl -a | grep -i disk

choose_disk=$(glabel list |\
		dmenu |\
		awk '{print $1}')

mount -t ext2fs /dev/ad1s1 /mnt

/dev/da0s1   /mnt/sdcard   ext4   mountprog=/usr/local/bin/ext4fuse,allow_other,late,rw   0   0
