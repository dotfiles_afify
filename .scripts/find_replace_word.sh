#!/bin/sh

dir=$1
old_word=$2
new_word=$3

find "$dir" -type f -exec sed -i "s/$old_word/$new_word/g" {} +
