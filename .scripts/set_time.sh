#!/bin/sh

[ "$USER" != "root" ] && echo "Must run as root." && exit 1
ntpd -s
hwclock --systohc
