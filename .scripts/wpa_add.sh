#!/bin/sh

wifi=$(ifconfig | awk '/wlp/ {print $1}' | tr -d ':')
ssid=$1
pass=$2

wpa_passphrase $ssid $pass >> /etc/wpa_supplicant/wpa_supplicant-$wifi.conf

wpa_cli -i wlp62s0 reconfigure
sv restart dhcpcd

# doas wpa_supplicant -B -i wlp62s0 -c /etc/wpa_supplicant/wpa_supplicant-wlp62s0.conf && doas dhcpcd wlp62s0
