#!/bin/sh

selected_key=
# gpg --list-keys --with-colons  | awk -F: '/fpr:/ {print $10}'
# https://raymii.org/s/articles/GPG_noninteractive_batch_sign_trust_and_send_gnupg_keys.html

select_key(){
	case $1 in
		public)  reg="pub" list="--list-public-keys" ;;
		private) reg="sec" list="--list-secret-keys" ;;
	esac
	case $(uname) in
		Linux)   datef="date --date=@" ;;
		OpenBSD) datef="date -r "      ;;
	esac
	ids=$(gpg2 $list --with-colons | awk -F: "/^$reg.*\$/ {print \$5}")
	formated=$(
		for each_id in $ids ;do
			name_date=$(gpg2 "$list" --with-colons $each_id |\
				awk -v datef="$datef" -F: '/^uid:.*$/\
					{(""datef""$6" +\"%a %d, %b %Y\" ")|\
					getline formated_date;
					print $10 " " formated_date}')
			echo "$each_id" "$name_date"
		done)
	selected_key=$(printf "%s" "$formated" | dmenu -l 10 | awk '{print $1}')
	if [ ! "$selected_key" ]; then exit; fi
}

select_all_keys(){
# 	selected_key=$(gpg2 --list-secret-keys --with-colons | awk -F: "/^sec.*\$/ {print \$5}")
	selected_key=$(gpg2 --list-secret-keys --with-colons | awk -F: '/^sec.*/ {print $5}')
}

create_new_ed25519_key(){
	echo "Key-Type: eddsa
	Key-Curve: ed25519
	Key-Usage: sign
	Subkey-Type: ecdh
	Subkey-Curve: cv25519
	Subkey-Usage: encrypt
	Expire-Date: 0
	Name-Real: Hassan Afify
	Name-Comment: $1
	Name-Email: hassan@afify.dev" | gpg2 --gen-key --batch
}

create_new_key(){
	echo "Key-Type: 1
	Key-Length: 4096
	Subkey-Type: 1
	Subkey-Length: 4096
	Expire-Date: 0
	Name-Real: Hassan Afify
	Name-Email: hassan@afify.dev" | gpg2 --gen-key --batch
}

delete_key(){
	pub_or_pri=$(printf "public\nprivate" | dmenu -p 'Choose key type: ')
	case $pub_or_pri in
		public)select_key public
			gpg2 --delete-key "$selected_key" ;;
		private)select_key private
			gpg2 --delete-secret-keys "$selected_key"
			d_pub=$(printf "yes\nno" | dmenu -p 'delete public key also ? ')
			if [ "$d_pub" = "yes" ]; then gpg2 --delete-key "$selected_key"; fi ;;
		*) exit ;;
	esac
}

print_full(){
	gpg2 --list-keys --keyid-format LONG --fingerprint --with-keygrip
	gpg2 --list-secret-keys --keyid-format LONG --fingerprint --with-keygrip
}

export_key(){
	pub_or_pri=$(printf "public\nprivate" | dmenu -p 'Choose key type: ')
	case $pub_or_pri in
		public) select_key public
			gpg2 --armor --export "$selected_key" ;;
		private) select_key private
			gpg2 --export-secret-keys "$selected_key" ;;
	esac
}

export_all(){
	select_all_keys
# 	for each_key in $selected_key ;do
# 		gpg2 --export-secret-keys $each_key >
# 	done
}

upload_to_server(){
	select_key private
	gpg2 --send-keys "$selected_key"
}

find_by_email(){
	gpg2 --keyserver keys.gnupg.net --search-key "$1"
}

import(){
	gpg2 --import "$1"
}


# encrypt_file(){
# 	if [[ -f $1 ]]; then
# 		filename=$1
# 		pass=$2
# 		# TODO encrypt with many public keys
# 		gpg2--recipient $pub_id  --encrypt $of.bz2
# # 		gpg2--yes --batch --passphrase=$pass -c $filename
# # 		gpg2--encrypt --recipient 'admin@example.com' --output confidential.txt.enc public.txt
# 	fi
# }
#
# decrypt_file(){
# 	if [[ -f $1 ]]; then
# 		# TODO  files end with .gpg
# 		filename=$1
# 		pass=$2
# 		# TODO
# 			gpg2--output $output --decrypt $filename
# # 		gpg2--yes --batch --passphrase=$pass $filename
# # 		gpg2--decrypt --output public.txt confidential.txt.enc
# 	fi
# }
#
sign_file(){
	select_key private
	gpg2 --default-key "$selected_key" --sign "$1"
}

verify_file(){
	gpg2 --verify "$1"
}

restart_agent(){
	gpg-connect-agent reloadagent /bye
}

edit_key(){
	select_key private
	gpg2 --edit-key "$selected_key"
}

case $1 in
	gen)     create_new_key   ;;
	gened)   create_new_ed25519_key "$2";;
	show)    print_full       ;;
	restart) restart_agent    ;;
	public)  select_key public
		echo "$selected_key"    ;;
	private) select_key private
		echo "$selected_key"    ;;
	delete)  delete_key       ;;
	edit)    edit_key         ;;
	export)  export_key "$2"    ;;
	export_all) export_all    ;;
	upload)  upload_to_server ;;
	find)    find_by_email "$2" ;;
	import)  import "$2"        ;;
# 	encrypt) encrypt_file $2  ;;
# 	decrypt) decrypt_file $2  ;;
	sign)    sign_file "$2"     ;;
	verify)  verify_file "$2"   ;;
esac
