#!/usr/bin/env bash

mkdir /tmp/suckless
cd /tmp/suckless

git clone https://git.suckless.org/dwm
diff -u  /tmp/suckless/dwm/config.def.h /mnt/data/dev/built/dwm-6.2/config.def.h > dwm.diff

git clone https://git.suckless.org/st
diff -u  /tmp/suckless/st/config.def.h /mnt/data/dev/built/st-0.8.2/config.def.h > st.diff

# patch -d/ -p0 < ~/dwm.diff
# make clean all
# doas make install
