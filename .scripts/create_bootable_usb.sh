#!/bin/sh

usb=$(\
	lsblk --noheadings --raw -o NAME,SIZE,MOUNTPOINT |\
	awk '$1~/sd[a-z]$/' |\
	dmenu -i -p "Choose disk"|\
	awk '{print $1}')
if [ ! "$usb" ]; then exit;fi


target="/mnt/4tb/os/"
if [ ! -d "$target" ]; then
	echo "please select iso file"
	target=$(echo '/home/hassan/' | dmenu -p 'enter full path')
# 	exit
fi

[ -z "$target" ] && target="$(realpath .)"
prompt="$2"
while true; do
	p="$prompt"
	[ -z "$p" ] && p="$target"
	sel="$(ls -1a "$target" | grep -v '^\.$' | dmenu -p "$p" -l 25)"
	ec=$?
	[ "$ec" -ne 0 ] && exit $ec
	c="$(echo "$sel" | cut -b1)"
	if [ "$c" = "/" ]; then
		newt="$sel"
	else
		newt="$(realpath "${target}/${sel}")"
	fi
	if [ -e "$newt" ]; then
		target="$newt"
		if [ ! -d "$target" ]; then

			# Got the iso file
			if dmenu -P -p "Warning Device will be erased | sudo " |\
				sudo -S dd bs=4M\
				if="$target" of="/dev/$usb"\
				status=progress oflag=sync; then
				notify-send "Bootable USB Created" "$usb"
			else
				notify-send -u critical\
					"Error Creating Bootable USB" "$usb"
			fi
			exit 0

		fi
	fi
done
