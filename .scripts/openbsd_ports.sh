#!/bin/sh

cd /tmp && ftp "https://cdn.openbsd.org/pub/OpenBSD/$(uname -r)/{ports.tar.gz,SHA256.sig}"
signify -Cp "/etc/signify/openbsd-$(uname -r | cut -c 1,3)-base.pub -x SHA256.sig ports.tar.gz"
cd /usr && doas tar xzf /tmp/ports.tar.gz
