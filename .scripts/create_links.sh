#!/bin/sh
ln -s /etc/sv/wpa_supplicant /var/service/
ln -s /bin/tcc /usr/local/bin/cc
ln -s /home/hassan/.vimrc /root/.vimrc
ln -s /home/hassan/.vim /root/.vim
ln -s /etc/sv/dbus /var/service/
ln -s /etc/sv/bluetoothd /var/service/
ln -s /mnt/data/todo.txt /home/hassan/
