#!/bin/sh

create(){
	filename="$1"
	password="$2"
	size="10m"
	encryption="AES"
	filetype="exfat"
	hash_type="sha-512"
	volume_type="normal"

	veracrypt -t --create "$filename" --filesystem "$filetype" \
		--encryption "$encryption" --password "$password" --size "$size" \
		--volume-type "$volume_type" --hash "$hash_type" --pim=0 -k "" \
		--random-source=/dev/urandom
}

case "$1" in
	create) create "$2" "$3";;
	mount)  veracrypt --mount filename /media/vera ;;
	umount) veracrypt -d ;;
	*) echo "create, mount, umount";;
esac
