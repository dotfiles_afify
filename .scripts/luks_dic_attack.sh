#!/usr/bin/env bash

#==============================================================================
# Name        : luks_dic_attack
# GitHub      : Afify
# Copyright   : MIT
# Version     : 0.0.1
# Description : Crack LUKS partion with hashcat with dictionary attack
#==============================================================================

chosen=$(\
	# 	awk '$1~/[[:digit:]]/ && $3~/[[:alnum:]]/ && $4 == "";\
	lsblk --noheadings --raw -o NAME,SIZE,FSTYPE,TYPE,MOUNTPOINT |\
	awk '$4 == "part" && $5 == "" && $3 == "crypto_LUKS"'|\
	dmenu -i -p "Mount volume" -l 10 )

partion=$(echo $chosen | awk '{print $1}')

if [ $partion ]; then
	dev=/dev/sdb1
	output_header_file=luks_header.001
# 	dictionary=/mnt/data/wordlist/rockyou.txt
	dictionary=/home/hassan/dic.txt
	cracked_password=cracked_password.txt

	# Get LUKS header
	luks_header=$(
		dmenu -P -p "Get Partition header | sudo " |\
		sudo -S dd if=$dev of=$output_header_file bs=512 count=4097 \
			&& notify-send "Success" "Got header file"\
			|| notify-send -u critical "Error" "Getting header file"
	)

	# Case got header file -> crack with hashcat
	if [ $luks_header ];then
		hashcat -m 14600 -a 0 -w 3\
		$output_header_file $dictionary -o $cracked_password \
			&& notify-send "Success" "Cracked"\
			|| notify-send -u critical "Error" "Crack failed"
	fi

fi
