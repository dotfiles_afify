#!/usr/bin/env bash

# mkdir -p /home/vm/hoth
# cd /home/vm/hoth
# vmctl create hoth.qcow2 -s 20G
# ftp http://archlinux.mirrors.ovh.net/archlinux/iso/2019.07.01/archlinux-2019.07.01-x86_64.iso
# more /etc/vm.conf
(...)
vm "template" {
  disable
  disk "/dev/null"
  memory 512M
  interface {
    group "vm"
    switch "vswitch0"
  }
}

# vmctl start hoth -c -t template -B cdrom -d ./hoth.qcow2 -r ./archlinux-2019.07.01-x86_64.iso

When the boot loader appears, hit <TAB>. The enter “ console=ttyS0,115200” and hit <ENTER>. This will boot Arch Linux using the console.

# vi /etc/vm.conf
(...)
vm "hoth" {
#  disable
  memory 4G
  disk "/home/vm/hoth/hoth.qcow2"
  interface {
    group "vm"
    locked lladdr fe:e1:ba:d2:50:75
    switch "vswitch0"
  }
}

# vmctl reload
# vmctl console hoth
Connected to /dev/ttyp4 (speed 115200)

hoth login: root
Password:
[root@hoth ~]#

