#!/bin/sh

echo "permit :wheel
permit nopass hassan cmd pkg_add
permit nopass hassan cmd pkg_info
permit nopass hassan cmd fw_update
permit nopass hassan cmd syspatch
permit nopass hassan cmd sysupgrade
permit nopass hassan cmd xbps-install
permit nopass hassan cmd lsblk
permit nopass hassan cmd reboot
permit nopass hassan cmd zzz
permit nopass hassan cmd shutdown" > /etc/doas.conf
