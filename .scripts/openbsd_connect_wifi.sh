#!/bin/sh

[ "$USER" != "root" ] && echo "Must run as root." && exit 1

interface=$(ifconfig | awk '/UP,BROADCAST,RUNNING/ {print $1}' | tr -d ':')

ssid=$1
pass=$2

ifconfig $interface up
ifconfig $interface nwid $ssid wpakey $pass
dhclient $interface

echo "join dlink wpakey YbFfyCsYJp
join moto wpakey 2dcc3fada3c8
dhcp"> /etc/hostname.athn0 hostname.urndis0 hostname.urtwn0
