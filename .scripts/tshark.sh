#!/usr/bin/env bash

# http.request.method=="POST"

interface=
add_to_group(){
	usermod -a -G wireshark $USER
}

select_int(){
	up_interfaces=$(ifconfig | grep "UP,BROADCAST" |\
		sed 's/://' | awk '{print $1}')
	interface=$(echo "$up_interfaces" | dmenu -l 10 )
}

find_pass(){
	select_int
	tshark -i $interface -Y 'http.request.method == POST and tcp contains "password"' | grep password
}

caputre_all(){
	select_int
# 	tshark -i $interface -w capture-output.pcap
	tshark -i $interface -w capture-output.pcap
}

read(){
	tshark -r $1
# 	tshark -r $1 -V -x > result.log
# 	tshark -r $1 -Y "http"
# 	tshark -r $1 -Y http.request -T fields -e http.host -e http.user_agent | sort | uniq -c | sort -n
# 	tshark -r $1 -Y http.request -T fields -e http.host -e ip.dst -e http.request.full_uri
# 	tshark -r $1 -T fields -e http.host -e ip.dst -e http.request.full_uri
}

read_ssl(){
	tshark -r $1 -q -o "ssl.keys_list:127.0.0.1,4443,http,server.pem" -z "follow,ssl,ascii,1"
}

dns(){
	select_int
# 	tshark -i $interface -f "src port 53" -n -T fields -e dns.qry.name -e dns.resp.addr
# 	tshark -i wlan0 -f "src port 53" -n -T fields -e frame.time -e ip.src -e ip.dst -e dns.qry.name -e dns.resp.addr
	tshark -i $interface -f "src port 53" -n -T fields -e dns.qry.name
}

agents(){
	select_int
	tshark -i $interface -Y http.request -T fields -e http.host -e http.user_agent
}

case $1 in
	pass) find_pass ;;
	all) caputre_all ;;
	agents) agents ;;
	read) read $2 ;;
	ssl) read_ssl $2 ;; 
	dns) dns ;;
esac
