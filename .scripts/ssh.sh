#!/bin/sh

email="hassan@afify.dev"
server=$2
ssh_dir=$HOME/.ssh
selected_key=

select_key(){
	case $1 in
		public)  ktype='public' ;;
		private) ktype='private' not='!' ;;
	esac
		selected_key=$(find $ssh_dir -type f $not -iname "*.pub" !\
			-name "config" ! -name "known_hosts" |\
			dmenu -l 10 -p "Select $ktype ssh key: ")
}

clipb(){
	tr -d '\n' | xclip -sel clip
}

case $1 in
	gen) time ssh-keygen -t ed25519 -a 100 -C $email -f $ssh_dir/$server
		clipb $ssh_dir/$server.pub
		printf "\n# $server.com\nHost $server.com
			IdentityFile $ssh_dir/$server" | tr -d '\t' >> $ssh_dir/config ;;
	rsa_gen) ssh-keygen -t rsa -b 4096 -C $email -f $ssh_dir/$server ;;
	public)  select_key public; cat $selected_key | clipb ;;
	private) select_key private; cat $selected_key | clipb ;;
	pass)    select_key private; ssh-keygen -p -f $selected_key ;;
	finger)  select_key public; ssh-keygen -l -E md5 -f $selected_key | clipb;;
	send) select_key public; ssh-copy-id -i $selected_key -f $server ;;
esac
