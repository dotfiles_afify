#!/usr/bin/env bash

You'll need to logout and log back in for those changes to take effect. There are also some kernel sysctls we'll need to bump up for desktop use. Add the following values to /etc/sysctl.conf. The shm variables are for my laptop, which has 16 GB of RAM. You should scale them accordingly for your machine.

/etc/sysctl.conf
# shared memory limits (chrome needs a ton)
kern.shminfo.shmall=3145728
kern.shminfo.shmmax=2147483647
kern.shminfo.shmmni=1024

# semaphores
kern.shminfo.shmseg=1024
kern.seminfo.semmns=4096
kern.seminfo.semmni=1024

kern.maxproc=32768
kern.maxfiles=65535
kern.bufcachepercent=90
kern.maxvnodes=262144
kern.somaxconn=2048

