#!/usr/bin/env bash

#==============================================================================
# Name        : cpu
# GitHub      : Afify
# Copyright   : MIT
# Version     : 0.1
# Description : print cpu percentage usage.
#==============================================================================

read cpu a b c previdle rest < /proc/stat
prevtotal=$((a+b+c+previdle))
sleep 0.5
read cpu a b c idle rest < /proc/stat
total=$((a+b+c+idle))
cpu=$((100*( (total-prevtotal) - (idle-previdle) ) / (total-prevtotal) ))
echo -e "$cpu%"

# Get top 10 process usage
# ps -eo pcpu,pid,user,args | sort -k 1 -r | head -10
