#!/usr/bin/env bash
# Create a luks partition
# TODO format disk

chosen=$(\
	lsblk --noheadings --raw -o NAME,SIZE,TYPE,MOUNTPOINT |\
	awk '$3 == "part" && $4 == ""' |\
	dmenu -i -p  "Warning disk will be erased |\
		Encrypt Disk with LUKS" -l 10 -sb red -fn bold)

partition=$(echo $chosen | awk '{print $1}')

if [ $partition ]; then
	if encrypt=$(dmenu -P -p "sudo: "|\
		sudo -S sh -c "\

		dmenu -P -p '$partition Password' |\
		cryptsetup -q --key-size 512 --hash sha512 luksFormat /dev/$partition;\
		dmenu -P -p 'Unlock $partition to Make Filesystem' |\
		cryptsetup luksOpen /dev/$partition $partition;\
		mkfs.ext4 /dev/mapper/$partition;\
		sudo cryptsetup luksClose /dev/mapper/$partition"); then

		notify-send "Encrypted" "$partition"
	else
		notify-send -u critical "Error Encrypting $partition" "$encrypt"
		exit 0
	fi
fi
