#!/bin/sh

change(){
	pass_dir="$HOME/.password-store"
	backup_dir="$HOME/pass_bk"
	[ -d "$backup_dir" ] || mkdir "$backup_dir"
	all=$(find "$pass_dir" -type f -iname "*.gpg" -printf '%f\n' | sed -n 's/\.gpg$//p')
	all_url=$(echo "$all" | awk -F: '{print $1}')

	for i in $all; do
		pass "$i" >> "$backup_dir/$i.txt"
		pass generate -f "$i" 25
	done

	firefox $all_url
}

case $1 in
	reg) pass generate --no-symbols -f "$2" 20;;
	change) change;;
	*) echo "(reg | change)"
esac
