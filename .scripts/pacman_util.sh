#!/bin/sh

# cache packages
sudo ls /var/cache/pacman/pkg/ | wc -l

# disk usage
du -sh /var/cache/pacman/pkg/

# remove uninstalled pachages
sudo pacman -Sc

# clean all cache
sudo pacman -Scc

# output installed to a file
pacman -Qe | awk '{print $1}' > installed.txt
