#!/usr/bin/env bash

# install Arch on ARM

ip="alarmpi"
new_user="hassan"
pass="12345"
ssid="wifiname"
ssh_key="$HOME/.ssh/raspi"
iso="/mnt/4tb/os/raspberry/ArchLinuxARM-rpi-latest.tar.gz"

if [[ ! -f "$iso" ]]; then
	# TODO
	echo "select other iso or download"
	# wget http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-latest.tar.gz
fi

# select disk
# select from unmouted
	# TODO
disk="sdc"

# format disk
echo -e "o\nn\np\n+100M\nt\nc\nn\np\nw\n" | fdisk /dev/$disk

sudo mkfs.vfat /dev/$disk'1'
sudo mkfs.ext4 /dev/$disk'2'
sudo mkdir /tmp/boot
sudo mkdir /tmp/root
sudo mount /dev/$disk'1' /tmp/boot/
sudo mount /dev/$disk'2' /tmp/root/
bsdtar -xpf $iso -C /tmp/root
mv /tmp/root/boot/* /tmp/boot/
sync
echo "disable.overscan=1" > /tmp/boot/config.txt

ln -s /usr/lib/systemd/system/dhcpcd@.service /tmp/root/etc/systemd/system/multi-user.target.wants/dhcpcd@wlan0.service
ln -s /usr/share/dhcpcd/hooks/10-wpa_supplicant /tmp/root/usr/lib/dhcpcd/dhcpcd-hooks/
echo "ctrl_interface=DIR=/var/run/wpa_supplicant" > /tmp/root/etc/wpa_supplicant/wpa_supplicant-wlan0.conf
wpa_passphrase "${ssid}" "${pass}" >> /tmp/root/etc/wpa_supplicant/wpa_supplicant-wlan0.conf
vim /tmp/root/etc/wpa_supplicant/wpa_supplicant-wlan0.conf # known bug (check pass)

mkdir /tmp/root/root/.ssh
chmod 0700 /tmp/root/root/.ssh
cat "$ssh_key" > /tmp/root/root/.ssh/authorized_keys
chmod 0600 /tmp/root/root/.ssh/authorized_keys
echo -e "# Raspberry pi\nHost $ip\nIdentityFile $ssh_key"> ~/.ssh/config
sudo umount /tmp/boot /tmp/root

### Stage 2
ssh root@$ip -i "$ssh_key"
pacman -Syyu
pacman-key --init
pacman-key --populate archlinuxarm

useradd -m -g users -s /bin/bash $new_user
passwd $new_user
echo "$new_user ALL=(ALL) ALL" >> /etc/sudoers

new_user_h="/home/$new_user"
mkdir $new_user_h/.ssh
cp .ssh/authorized_keys $new_user_h/.ssh
chown $new_user $new_user_h/.ssh/authorized_keys
chmod 700 $new_user_h/.ssh/authorized_keys

pacman -S --needed - < .scripts/pacman_arm.txt
systemctl enable nginx
systemctl enable cronie
systemctl start nginx
systemctl start cronie
