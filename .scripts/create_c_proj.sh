#!/bin/sh

# Create a C project:
# 	git init + files
# 	license
# 	README
# 	Makefile
# 	config.mk
# 	man page
# 	gitignore
# 	config.def.h
# 	c file
# 	util.c
# 	util.h
usage() {
	echo "usage create_c_proj.sh <proj_name>"
}

[ ! $1 ] && usage && exit;
proj=$1
year=$(date +'%Y')
mkdir "$proj"
cd "$proj" && git init > /dev/null
git remote add github git@github.com:afify/"$proj".git > /dev/null
git remote add repo ssh://repo.or.cz/"$proj".git > /dev/null

# Create gitignore
#==============================================================================
printf "%s\\n*.db\\n*.txt\\n*.log\\n*.o\\n.gitignore" "$proj" > ./.gitignore

# Create License
#==============================================================================
echo "ISC License

© $year Hassan Afify <hassan at afify dot dev>

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE." > ./LICENSE

# Create Makefile
#==============================================================================
echo "# $proj
# See LICENSE file for copyright and license details.

include config.mk

SRC = $proj.c util.c
OBJ = \${SRC:.c=.o}

all: options $proj

options:
	@echo $proj build options:
	@echo \"CFLAGS   = \${CFLAGS}\"
	@echo \"LDFLAGS  = \${LDFLAGS}\"
	@echo \"CC       = \${CC}\"

.c.o:
	\${CC} -c \${CFLAGS} \$<

\${OBJ}: config.h config.mk

config.h:
	cp config.def.h \$@

$proj: \${OBJ}
	\${CC} -o \$@ \${OBJ} \${LDFLAGS}

clean:
	rm -f $proj \${OBJ} $proj-\${VERSION}.tar.gz

dist: clean
	mkdir -p $proj-\${VERSION}
	cp -R LICENSE Makefile README.md config.def.h config.mk\\
		$proj.1 util.h \${SRC} $proj-\${VERSION}
	tar -cf $proj-\${VERSION}.tar $proj-\${VERSION}
	gzip $proj-\${VERSION}.tar
	rm -rf $proj-\${VERSION}

install: $proj
	mkdir -p \${DESTDIR}\${PREFIX}/bin
	cp -f $proj \${DESTDIR}\${PREFIX}/bin
	chmod 755 \${DESTDIR}\${PREFIX}/bin/$proj
	mkdir -p \${DESTDIR}\${MANPREFIX}/man1
	sed \"s/VERSION/\${VERSION}/g\" < $proj.1 > \${DESTDIR}\${MANPREFIX}/man1/$proj.1
	chmod 644 \${DESTDIR}\${MANPREFIX}/man1/$proj.1

uninstall:
	rm -f \${DESTDIR}\${PREFIX}/bin/$proj\\
		\${DESTDIR}\${MANPREFIX}/man1/$proj.1

.PHONY: all options clean dist install uninstall"> ./Makefile

# Create Readme
#==============================================================================
echo "$proj
====
[![Language grade: C/C++]()]()
[![Build status]()]()
[![License]()]()

$proj is a simple {description} for unix-like systems, follows
the suckless [philosophy](https://suckless.org/philosophy/) and
[code style](https://suckless.org/coding_style/).

**current**
\`\`\`sh
git clone https://github.com/afify/$proj.git
cd $proj/
make
make install
\`\`\`
**latest release**
\`\`\`sh
wget --content-disposition \$(curl -s https://api.github.com/repos/afify/$proj/releases/latest | tr -d '\",' | awk '/tag_name/ {print \"https://github.com/afify/$proj/archive/\"\$2\".tar.gz\"}')
tar -xzf $proj-*.tar.gz && cd $proj-*/
make
make install
\`\`\`
Run
---
\`\`\`sh
\$ $proj
\`\`\`
Options
-------
\`\`\`sh
$ $proj [-v]
$ man $proj
\`\`\`
| option | description                                  |
|:------:|:---------------------------------------------|
| \`-v\`   | print version.                               |

Configuration
-------------
The configuration of $proj is done by creating a custom config.h
and (re)compiling the source code." > ./README.md

# Create config.def
#==============================================================================
printf "/* See LICENSE file for copyright and license details.*/\\n
#ifndef CONFIG_H\\n#define CONFIG_H\\n\\n\\n\\n#endif /* CONFIG_H */\\n"> ./config.def.h

# Create config.mk
#==============================================================================
printf "# %s version
VERSION = 0.1

# Customize below to fit your system

# paths
PREFIX    = /usr/local
MANPREFIX = \${PREFIX}/share/man

#includes and libs
STD  = -std=c99
WARN = -pedantic -Wextra -Wall
LIBS =

# flags
CPPFLAGS = -D_DEFAULT_SOURCE -D_BSD_SOURCE -D_POSIX_C_SOURCE=200809L -DVERSION=\\\\\"\${VERSION}\\\\\"
CFLAGS   = \${STD} \${WARN} -Os \${CPPFLAGS}
LDFLAGS  = \${LIBS}

# compiler and linker
CC = cc" "$proj" > ./config.mk

# Create man page
#==============================================================================
echo ".TH $proj 1 $proj\-VERSION
.SH NAME
$proj \- simple
.SH SYNOPSIS
.B $proj
.RB [ \-hv ]
.SH DESCRIPTION
$proj is a simple
.P
.SH OPTIONS
.TP
.B \-h
prints usage help.
.TP
.B \-v
print version.
.SH USAGE
.TP
.B $proj
.SH CUSTOMIZATION
$proj is customized by creating a custom config.h and (re)compiling the source
code. This keeps it fast, secure and simple.
.SH AUTHORS
Hassan Afify <hassan at afify dot dev>"> ./"$proj".1

# Create c file
#==============================================================================
printf "/* See LICENSE file for copyright and license details. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include \"config.h\"
#include \"util.h\"

/* macros */
/* typedef */
/* function declarations */
static void start(void);

/* global variables */

/* function implementations */
static void
start(void)
{
	die(\"%s\");
}

int
main(int argc, char *argv[])
{
#ifdef __OpenBSD__
	if (pledge(\"stdio\", NULL) == -1)
		die(\"pledge\");
#endif /* __OpenBSD__ */
	if (argc == 1)
		start();
	else if (argc == 2 && strncmp(\"-v\", argv[1], 2) == 0)
		die(\"%s-\" VERSION);
	else
		die(\"usage: %s [-v]\");
	return 0;
}\\n" "$proj" "$proj" "$proj" > ./"$proj".c

# Create util.c file
#==============================================================================
printf  "/* See LICENSE file for copyright and license details. */
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include \"util.h\"

void *
ecalloc(size_t nmemb, size_t size)
{
	void *p;
	p = calloc(nmemb, size);
	FAIL_IF(p == NULL, \"calloc\");
	return p;
}

void
die(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	(void)vfprintf(stderr, fmt, ap);
	va_end(ap);

	if (fmt[0] != '\\\0' && fmt[strlen(fmt)-1] == ':') {
		(void)fputc(' ', stderr);
		perror(NULL);
	} else {
		(void)fputc('\\\n', stderr);
	}

	exit(EXIT_FAILURE);
}\\n">./util.c

# Create util.h file
#==============================================================================
printf "/* See LICENSE file for copyright and license details. */

#ifndef UTIL_H
#define UTIL_H

#define MAX(A, B)               ((A) > (B) ? (A) : (B))
#define MIN(A, B)               ((A) < (B) ? (A) : (B))
#define BETWEEN(X, A, B)        ((A) <= (X) && (X) <= (B))
#define FAIL_IF(EXP, MSG)       {if(EXP){fprintf(stderr, \"[\\\033[31mFAILED \\%d\\\033[0m] \\%s\\\n\", __LINE__, MSG);exit(EXIT_FAILURE);}}

void *ecalloc(size_t, size_t);
void die(const char *fmt, ...);

#endif /* UTIL_H */\\n">./util.h
