# vim: filetype=neomuttrc

set folder           = "~/.mail"
set header_cache     = "~/.config/mutt/cache/headers"
set message_cachedir = "~/.config/mutt/cache/bodies"
set certificate_file = "~/.config/mutt/certificates"
set mailcap_path     = "~/.config/mutt/.mailcap"
set tmpdir           = "~/.config/mutt/tmp"

set ssl_force_tls       = yes

source ~/.config/mutt/accounts/hassan
source ~/.config/mutt/theme
source ~/.config/mutt/keymap

# basic options
set editor = "vim"
set charset = "utf-8"
set assumed_charset = "utf-8"
set attach_charset = "utf-8"
set wait_key = no
set delete
set quit
set thorough_search
set mail_check_stats
unset confirmappend
unset move
unset mark_old
unset beep_new
set status_on_top # Status bar on top.
set new_mail_command="notify-send 'New Email' '%n new messages, %u unread.' &"
set mbox_type = Maildir

# Sync and check intervals
set read_inc = 100
set mail_check = 0
set timeout = 3

# Compose View Options
set use_envelope_from                    # which from?
set edit_headers                     # show headers when composing
set fast_reply                       # skip to compose when replying
set askcc                            # ask for CC:
set fcc_attach                       # save attachments with the body
set forward_format = "Fwd: %s"       # format of subject when forwarding
set forward_decode                   # decode when forwarding
set attribution = "On %d, %n wrote:" # format of quoting header
set reply_to                         # reply to Reply to: field
set include                          # include message in replies
set forward_quote                    # include message in forwards
set text_flowed
unset mime_forward                   # forward attachments as part of body

# status bar, date format, finding stuff etc.
set status_chars  = " *%A"
set status_format = "[ Folder: %f ] [%r%m messages%?n? (%n new)?%?d? (%d to delete)?%?t? (%t tagged)? ]%>─%?p?( %p postponed )?"
set date_format   = "%d.%m.%Y %H:%M"
# set index_format = "%2C %Z [%{%m/%d}] %-30.30F (%-4.4c) %s"
set index_format = "%Z %{%d/%m} %-30.30F %-85.85s %-4.4c"

set sort          = threads
set sort_aux      = reverse-last-date-received
set uncollapse_jump
set sort_re
set reply_regexp  = "^(([Rr][Ee]?(\[[0-9]+\])?: *)?(\[[^]]+\] *)?)*"
set quote_regexp  = "^( {0,4}[>|:#%]| {0,4}[a-z0-9]+[>|]+)+"
set send_charset  = "utf-8:iso-8859-1:us-ascii"
set user_agent = no

# Pager View Options
# set pager_index_lines = 10
set pager_context = 3
set pager_stop
set menu_scroll
set tilde
unset markers

# email headers and attachments
ignore *
unignore from subject to cc date x-mailer x-url user-agent
unhdr_order *
hdr_order from to cc date subject x-mailer user-agent
alternative_order text/plain
# auto_view text/html
set abort_nosubject=no
set attribution = " %n wrote:\n" # I don't like lengthy attributions as well

# sidebar
set sidebar_visible
set sidebar_short_path
set sidebar_folder_indent
set sidebar_width = 25
set sidebar_divider_char = ' │ '
set sidebar_indent_string = '  ''
# set sidebar_format = "[%?N?%N]"
set sidebar_format = "%B %* [%?N?%N / ?%S]"
# set sidebar_format="%B%?F? [%F]?%* %?N?%N/?%S"
# color sidebar_new yellow default

# GPG/PGP
set pgp_sign_as        = `cat ~/.password-store/.gpg-id`
set pgp_use_gpg_agent  = yes
set crypt_use_gpgme    = yes
set crypt_autosign     = no
set crypt_autoencrypt  = no
set crypt_verify_sig   = yes
set crypt_replysign    = yes
set crypt_replyencrypt = yes
set crypt_replysignencrypted = yes
